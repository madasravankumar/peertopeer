//
//  PeerToPeerFlow.swift
//  PeerToPeer
//
//  Created by Sravan kumar on 12/8/21.
//

import Foundation

public class PeerToPeerFlow {
    let firstName: String
    let lastName: String
    public init(firstName: String, lastName: String) {
        self.firstName = firstName
        self.lastName = lastName
    }
    
    public func description() {
        print(firstName, lastName)
    }
}
